//===========Problem4 = find =====================




// function find(elements, cb) {
//     // Do NOT use .includes, to complete this function.
//     // Look through each value in `elements` and pass each element to `cb`.
//     // If `cb` returns `true` then return that element.
//     // Return `undefined` if no elements pass the truth test.
// }



//=====================================================================================

const items = [1, 2, 3, 4, 5, 5]; 


function find(array, cb){
    let searchingElement;
    for(let index = 0; index < array.length; index++){
        const condition = cb(array[index]);
        if(condition === true){
            searchingElement = array[index];
            break;
        }
    }
    return searchingElement;
}



module.exports = find;








