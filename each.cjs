//===========Problem1 = forEach =====================


// function each(elements, cb) {
//     // Do NOT use forEach to complete this function.
//     // Iterates over a list of elements, yielding each in turn to the `cb` function.
//     // This only needs to work with arrays.
//     // You should also pass the index into `cb` as the second argument
//     // based off http://underscorejs.org/#each
// }


//=====================================================================================


// const items = [1, 2, 3, 4, 5, 5]; 

function each(array, cb){
    for(let index = 0; index < array.length; index++){
        cb(array[index], index, array);
    }
    return ;
}

// function cb(ele, ind, arr){
//     return ;
// }
// console.log(items);

// each(items, cb)


module.exports = each;







