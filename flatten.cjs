//===========Problem6 = flatten =====================


// const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

// function flatten(elements) {
//     // Flattens a nested array (the nesting can be to any depth).
//     // Hint: You can solve this using recursion.
//     // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
// }
// const arr = [2, 3, 4, [5], [[6]], [[[7]]], [[[[8]]]], , , []];


//=====================================================================================

// const arr = [2, 3, 4, [5], [[6]], [[[7]]], [[[[8]]]], , , []];

// const arr = [1, 2, 3, [1, 2]];


function flatten(arr, depth=1){

    const newArray = [];//[1,2,3]


    for(let index = 0; index < arr.length; index++){
        // console.log(index in arr);
        
        if(Array.isArray(arr[index]) && depth > 0){
            const nestedArray = flatten(arr[index] , depth-1);
            // console.log(nestedArray);
            for(let inner = 0; inner < nestedArray.length; inner++){
                newArray.push(nestedArray[inner]);
            }
        }
        else if(index in arr){
            newArray.push(arr[index]);
            // console.log(index, newArray);
        }
    }
    return newArray;
}


module.exports = flatten;





