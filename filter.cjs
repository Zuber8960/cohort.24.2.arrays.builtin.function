//===========Problem5 = filter =====================



// function filter(elements, cb) {
//     // Do NOT use .filter, to complete this function.
//     // Similar to `find` but you will return an array of all elements that passed the truth test
//     // Return an empty array if no elements pass the truth test
// }


//=====================================================================================

// const items = [1, 2, 3, 4, 5, 5]; 


function filter(array, cb){
    let newArray = [];
    for(let index = 0; index < array.length; index++){
        const condition = cb(array[index], index, array);
        if(condition === true){
            newArray.push(array[index]);
        }
    }
    return newArray;
}



module.exports = filter;
