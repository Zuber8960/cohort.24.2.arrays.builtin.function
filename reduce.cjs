
//===========Problem3 = reduce =====================


// function reduce(elements, cb, startingValue) {
//     // Do NOT use .reduce to complete this function.
//     // How reduce works: A reduce function combines all elements into a single value going from left to right.
//     // Elements will be passed one by one into `cb` along with the `startingValue`.
//     // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
//     // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
// }



//=====================================================================================

const items = [1, 2, 3, 4, 5];


function reduce(array, cb, startingValue){
    let flag = true;
    if(startingValue === undefined){
        flag = false;
        startingValue = array[0];
    }
    for(let index = 0; index < array.length; index++){
        if(index === 0 && flag === false){
            index = 1;
        }
        startingValue = cb(startingValue, array[index], index, array);
    }
    return startingValue;
}



module.exports = reduce;



