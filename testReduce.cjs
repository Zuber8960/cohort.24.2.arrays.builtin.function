const reduce = require('./reduce.cjs');

const items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// const items = ["Hi", "THis", "is", "a" , "Zuber", "Ahmad"]; 


function cb(acc , curr, ind, dummyArr){
    return acc+curr;
}


// function cb(acc , curr, ind, dummyArr){
//     console.log(acc, curr, ind, dummyArr);
// }


// const result = reduce(items, cb);
// const result = reduce(items, cb, 100);
const result = reduce(items, cb);

console.log(result);